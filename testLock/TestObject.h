//
//  TestObject.h
//  testLock
//
//  Created by Open on 5/15/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestObject : NSObject

-(void)method1;
-(void)method2;

@end
